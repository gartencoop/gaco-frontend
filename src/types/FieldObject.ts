export type FieldObject = {
    name: string;
    key: string;
    fieldType: "text" | "checkbox" | "largeText" | "select" | "date" | "boolean" | "money";
    subtle?: string;
    width?: number;
    options?: {
        name: string;
        value: string;
    }[],
    disabled?: (userInfos: any) => boolean;
}