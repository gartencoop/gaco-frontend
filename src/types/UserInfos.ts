export type UserInfos = {
    city: string;
    create_at: string;
    id: number;
    name: string;
    surname: string;
    notes: string
    deposit_cents: number | null,
    [key: string]: string | number | boolean;
  }
  