export type Bidding = {
    euros: number;
    change_at: string;
    start_at: string;
    index?: number;
}