export type BidYear = {
    euros: number;
    year: number;
    months: number;
}