export const openSearchEvent = new EventTarget();

type GlobalSearchProps = {
    search?: string;
    searchType?: "documents" | "events" | "both";
    document_team?: string;
    document_topic?: string;
    event_category?: string;
}

export const useGlobalSearch = () => {
    const openSearch = (props: GlobalSearchProps) => {
        openSearchEvent.dispatchEvent(new CustomEvent('openSearch', { detail: props }));
    };

    const registerEventHandler = (callback: (event: CustomEvent) => void) => {
        openSearchEvent.addEventListener('openSearch', callback);
    };

    const unregisterEventHandler = (callback: (event: CustomEvent) => void) => {
        openSearchEvent.removeEventListener('openSearch', callback);
    };

    return { openSearch, registerEventHandler, unregisterEventHandler };
}
