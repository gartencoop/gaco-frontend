const { createProxyMiddleware } = require("http-proxy-middleware");
const http = require("node:http");

//const target = "https://test.gaco.uber.space";
//const target = "http://192.168.178.164:1111";
const target = "http://127.0.0.1:1111";

process.on('uncaughtException', function (err) {
  console.log(err);
});

module.exports = function (app) {
  const static_proxy = createProxyMiddleware({ target, changeOrigin: true });
  app.use("/image/*", static_proxy);
  app.use("/ics/*", static_proxy);
  app.use("/file/*", static_proxy);
  app.use("/csv/*", static_proxy);
};
