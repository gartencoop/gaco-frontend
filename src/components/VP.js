import { Box, Typography, Stack, Grid, useMediaQuery, Divider } from "@mui/material";
import styled from "@emotion/styled";
import theme from "../theme";
import Editor from "./modules/Editor";
import ImageSlider from "./modules/ImageSlider";
import { url } from "../helper/websocketHelper";
import ImageModal from "./modules/ImageModal";
import { Download } from "@mui/icons-material";

function VP({ data }) {
  const document = data.interaction.vp.value;
  const texts = data.interaction.texts.value;
  const showAside = document.images?.length + document.files?.length
  const tablet = useMediaQuery(theme.breakpoints.up('lg'));
  return (
    <Box className="vp">
      <Grid container>
        <Grid item xs={12} md={8}>
          <Typography variant="h2">{texts.title}</Typography>
          {document.opening && (
            <Typography variant="h4">{texts.opening_hours}: <br /> {document.opening}</Typography>
          )}

          <Stack className="vp__infobox">
            <Box>
              {document.code && <Typography><strong>{texts.code}:</strong> {document.code}</Typography>}
              {(
                <Typography><strong>{texts.members}:</strong> {document.members_count}</Typography>
              )}
              {(
                <Typography><strong>{texts.waiting_members}:</strong> {document.waiting_members_count}</Typography>
              )}
              {document.contact && (
                <Typography><strong>{texts.contact}:</strong> {document.contact}</Typography>
              )}
            </Box>
            <Box>
              {document.case_storage && (
                <Typography><strong>{texts.case_storage}:</strong> {document.case_storage}</Typography>
              )}
              {document.created_at && (
                <Typography><strong>{texts.created_at}</strong> {new Date(document.created_at).toLocaleDateString('de-DE', {
                  year: "numeric",
                  month: "2-digit",
                  day: "2-digit",
                })}</Typography>
              )}
              {document.status && (
                <Typography><strong>{texts.status}:</strong> {document.status}</Typography>
              )}
              {document.space && <Typography>{texts.space}: {document.space}</Typography>}
            </Box>
          </Stack>
          {document.body && (
            <>
              <Typography variant="h2">{texts.infos}</Typography>
              <Editor body={document.body} />
            </>
          )}
          {document.members.length > 0 && (
            <>
              <Typography variant="h2">{texts.members}</Typography>
              <ul>
                {document.members.map((name, index) => (
                  <li key={index}>{name}</li>
                ))}
              </ul>
            </>
          )}
        </Grid>
        <Grid item xs={12} md={4}>
          {showAside > 0 && (
            <Aside>
              {tablet && document.images.length > 0 && (
                <>
                  <Typography variant="h5" sx={{ margin: "20px" }}>
                    Bilder
                  </Typography>
                  {document.images.map((image, index) => {
                    // TODO: image link muss hier noch korrigiert werden
                    return (
                      <ImageModal
                        key={index}
                        image={image}
                        index={index}
                        images={document.images}
                      />
                    );
                  })}
                </>
              )}
              {!tablet && document.images.length > 0 && (
                <ImageSlider images={document.images} />
              )}
              {document.images.length > 0 && document.files.length > 0 && (
                <Divider sx={{ color: theme.palette.darkgreen }} />
              )}
              {document.files.length > 0 && (
                <>
                  <Typography variant="h5" className="headline">
                    Dateien
                  </Typography>
                  {document.files.map((file, index) => {
                    return (
                      <FileDownload key={index} href={`${url}${file.path}`}>
                        <Download />
                        <p>{file.name}</p>
                      </FileDownload>
                    );
                  })}
                </>
              )}
            </Aside>
          )}
        </Grid>
      </Grid>
    </Box>
  );
}

export default VP;


const Wrapper = styled(Stack)(() => ({
  padding: "0 20px",

  [theme.breakpoints.up("md")]: {
    padding: "20px 0",
  },

  ".vp": {
    "&__infobox": {
      flexDirection: "row",
      flexWrap: "wrap",
      margin: "0 20px 20px",

      [theme.breakpoints.up("md")]: {
        gap: 100,
      },
      "& p": {
        paddingBottom: "10px",

        "& strong": {
          display: "inline",
        }
      }
    },
  },

  h4: {
    margin: "0 20px 20px",
  },
}));

const Aside = styled(Box)(() => ({
  boxSizing: "border-box",
  width: "100%",
  padding: "0 10px",

  "& h5": {
    display: "none",
  },

  [theme.breakpoints.up("md")]: {
    // width: "25%",
    padding: "0 0 0 30px",
    flex: "1 0 25%",

    "& h5": {
      display: "block",
      margin: "20px",
    },
  },
}));

const FileDownload = styled("a")(() => ({
  color: theme.palette.black,
  display: "flex",
  alignItems: "center",
  gap: 20,
  background: theme.palette.lightgreen,
  padding: 20,
  marginBottom: 20,
  textDecorationColor: "transparent",
  transition: "text-decoration-color 350ms ease-in-out",

  "& p": {
    wordBreak: "break-all",
    overflowWrap: "break-word",
    textWrap: "wrap",
  },
  "& svg": {
    transform: "scale(1)",
    transition: "transform 350ms ease",
  },
  "&:hover": {
    transition: "text-decoration-color 350ms ease-in-out",
    textDecorationColor: theme.palette.black,
    "& svg": {
      transform: "scale(1.5)",
      transition: "transform 350ms ease",
    },
  },
}));