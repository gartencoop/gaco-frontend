import { Grid } from '@mui/material';
import React from 'react'
import Card from './Card';

function Page({ data }) {
  const obj = data.interaction.posts.data.rows;

  return (
    <>
      <Grid>
        {obj.map((e, index) => <Card key={index} screendata={e.interaction} />)}
      </Grid>
    </>
  )
}

export default Page