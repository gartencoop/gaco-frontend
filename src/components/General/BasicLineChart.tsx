import React from 'react';
import { Line } from '@ant-design/plots';

export type BasicLineChartProps = {
    data: {
        date: string;
        value: number;
    }[]
    valueFormatter?: (value: number) => string;
}

export const BasicLineChart = ({ data, valueFormatter }: BasicLineChartProps) => {

    const config = {
        data,
        xField: 'date',
        yField: 'value',
        label: {
            formatter: valueFormatter,
        },
        point: {
            size: 5,
            shape: 'diamond',
            style: {
                fill: 'white',
                stroke: '#5B8FF9',
                lineWidth: 2,
            },
        },
        tooltip: {
            showMarkers: false,
        },
        state: {
            active: {
                style: {
                    shadowBlur: 4,
                    stroke: '#000',
                    fill: 'red',
                },
            },
        },
        interactions: [
            {
                type: 'marker-active',
            },
        ],
    };
    return <Line {...config} />;
};