import { Box, Dialog, IconButton, MenuItem, Paper, Select, Stack, TextField, Typography } from '@mui/material'
import { useEffect, useState } from 'react'
import styled from '@emotion/styled';
import theme from '../../theme';
import EditIcon from '@mui/icons-material/Edit';
import CloseIcon from '@mui/icons-material/Close';
import SaveIcon from '@mui/icons-material/Save';
import Editor from './Editor';
import { sendToBackend } from '../../helper/websocketHelper';
import ImageModal from './ImageModal';
import { AddToDrive, Delete } from '@mui/icons-material';

function EditDocument({ document: doc, teams: availableTeams, availableTopics }) {
  const [editOpen, setEditOpen] = useState(false);
  const [markdown, setMarkdown] = useState(doc?.body);
  const [title, setTitle] = useState(doc?.title);
  const [topic, setTopic] = useState(doc?.topic);
  const [teams, setTeams] = useState(doc?.teams);
  const touch = () => {
    setEditOpen((editOpen) => {
      if (!editOpen) {
        return editOpen;
      }

      sendToBackend("document", {
        type: "touch",
      });

      setTimeout(touch, 10000);

      return editOpen;
    });
  };

  useEffect(() => {
    //console.log("Teams", availableTeams, doc?.teams)
    setMarkdown(doc?.body);
    setTeams(doc?.teams);
    setTitle(doc?.title);
    setTopic(doc?.topic);
  }, [doc]);


  const openChoseFileDialog = () => {
    const input = document.createElement("input");
    input.type = "file";
    input.accept = "*";
    input.onchange = (e) => {
      const file = e.target.files[0];
      const reader = new FileReader();
      reader.onload = (e) => {
        try {
          const data = e.target.result;
          sendToBackend("document", {
            type: "add_file",
            name: file.name,
            data: data,
          });
        } catch (error) {
          console.warn("Error while uploading file: ", error);
        }
      };
      reader.readAsDataURL(file);
    };
    input.click();
  }

  return (
    <Box>
      <EditIcon onClick={() => {
        sendToBackend("document", {
          type: "edit",
        });
        setEditOpen(true);
        setTimeout(touch, 10000);
      }} />
      <Dialog
        open={editOpen}
        onClose={() => {
          sendToBackend("document", {
            type: "close"
          });
          setEditOpen(false);
        }}
        fullScreen={true}
        fullWidth
        maxWidth="xl"
      >
        <ButtonBox>
          <StyledButton onClick={() => {
            console.log("Submit to backend: ", markdown);
            sendToBackend("document", {
              type: "save",
              html: markdown,
              title: title,
              topic: topic,
              teams: teams,
            });
            sendToBackend("document", {
              type: "close",
            });
            setEditOpen(false);
          }}>
            <SaveIcon />
          </StyledButton>
          <StyledButton onClick={() => {
            sendToBackend("document", {
              type: "close",
            });
            setEditOpen(false);
          }}>
            <CloseIcon />
          </StyledButton>
        </ButtonBox>
        <Box sx={{ margin: 2 }}>
          <Stack direction={"column"} spacing={1} >
            <Typography>Titel</Typography>
            <TextField
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              fullWidth
            />
            <Typography>Thema</Typography>
            <Select value={topic} onChange={(e) => setTopic(e.target.value)} fullWidth>
              {(availableTopics?.value ?? [])?.map((t) => {
                return (
                  <MenuItem value={t}>{t}</MenuItem>
                );
              })}
            </Select>
            <Typography>Teams</Typography>
            <Select multiple value={teams} onChange={(e) => setTeams(e.target.value)} fullWidth>
              {(availableTeams?.value ?? [])?.map((t) => {
                return (
                  <MenuItem value={t.value}>{t.caption}</MenuItem>
                );
              })}
            </Select>
            <p>
              {getErrorMessageForEditValue(doc?.edit_mode?.value)}
            </p>
          </Stack>

          <Stack sx={{ marginTop: 2, marginBottom: 2 }} direction="row" spacing={2}>
            {doc?.images.map((image, index) => {
              return (
                <Paper elevation={2}>
                  <ImageModal
                    key={index}
                    image={image}
                    index={index}
                    images={doc.images}
                    width={"5vw"}
                    height={"5vw"}
                  />
                  <Stack direction={"row"} spacing={2} justifyContent={"end"}>
                    <IconButton color="error" onClick={() => {
                      sendToBackend("document", {
                        type: "remove_file",
                        path: image,
                      });
                    }}>
                      <Delete />
                    </IconButton>
                  </Stack>
                </Paper>
              );
            }
            )}
            {doc?.files.map((file, index) => {
              return (
                <Paper sx={{
                  width: "5vw",
                  height: "7.2vw",
                }} elevation={2}>
                  <Stack direction={"column"} sx={{ height: "100%" }} spacing={2} justifyContent={"space-between"} alignItems={"space-between"}>
                    <Typography sx={{
                      padding: 2,
                      wordWrap: 'break-word',  // Ensures long words break and wrap
                      overflowWrap: 'break-word',  // Alternative for better support
                      whiteSpace: 'normal'
                    }}>{file.name}</Typography>
                    <Stack direction={"row"} justifyContent={"end"}>
                      <IconButton color="error" onClick={() => {
                        sendToBackend("document", {
                          type: "remove_file",
                          path: file.path,
                        });
                      }}>
                        <Delete />
                      </IconButton>
                    </Stack>
                  </Stack>
                </Paper>
              );
            }
            )}
            <Paper sx={{
              width: "5vw", height: "7.2vw", ":hover": {
                backgroundColor: theme.palette.secondary.main
              }
            }} onClick={() => openChoseFileDialog()} elevation={2}>
              <Stack sx={{ height: "90%" }} direction={"column"} spacing={2} justifyContent={"center"} alignItems={"center"}>
                <AddToDrive sx={{ fontSize: "60px" }} />
              </Stack>
            </Paper>
          </Stack>
          <Editor body={markdown} id={doc?.id} onChange={(dt) => setMarkdown(dt)} />
        </Box>
      </Dialog >
    </Box >
  )
}

const getErrorMessageForEditValue = (editValue) => {
  switch (editValue) {
    case 'yes':
      return 'Editier Modus Aktiv!';
    case 'no':
      return 'Du bist nicht im Editor modus...';
    case 'lockedByOtherUser':
      return 'Ein anderer Nutzer bearbeitet gerade das Dokument!';
    default:
      console.log("Missing translation for editValue: ", editValue);
      return 'Unbekannter Fehler';
  }
}

export default EditDocument

// Styling
const StyledButton = styled(IconButton)(() => ({
  color: theme.palette.black,
  background: `${theme.palette.stone}44`,
  transition: "transform 150ms ease-in-out, background 350ms ease-in-out",
  "&:hover": {
    transform: "scale(1.05)",
    transition: "transform 150ms ease-in-out, background 350ms ease-in-out",
    background: `${theme.palette.stone}ff`
  }
}));

const ButtonBox = styled(Box)(() => ({
  padding: "30px 100px 30px 30px",
  display: "flex",
  alignItems: "center",
  gap: 30,
  justifyContent: "right"
}));

