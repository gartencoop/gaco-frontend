import { Typography } from '@mui/material';
import React from 'react';
import theme from '../theme';
import styled from '@emotion/styled';


const Headline = styled(Typography)(({}) => ({
  width: 300,
  background: theme.palette.yellow,
  color: theme.palette.brown,
  fontSize: 25,
}));


function Card({ screendata }) {
  return (
    <>
    <Headline>
      {screendata.author.value}
    </Headline>
    <div>
      {screendata.text.html}
    </div>
  </>
  )
}

export default Card