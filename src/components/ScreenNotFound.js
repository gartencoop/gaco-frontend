import React from 'react'

function ScreenNotFound({ name }) {
  return (
    <div>Screen not found: {name}</div>
  )
}

export default ScreenNotFound