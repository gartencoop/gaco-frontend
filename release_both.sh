#!/bin/bash
set -e

npm run build
rsync -avz --delete build/ gaco@erinome.uberspace.de:assets-live
rsync -avz --delete build/ gaco@erinome.uberspace.de:assets-test